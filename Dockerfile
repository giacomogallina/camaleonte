FROM archlinux

WORKDIR /camaleonte

COPY target/release/camaleonte .
COPY Rocket.toml .
COPY static ./static

CMD /camaleonte/camaleonte
