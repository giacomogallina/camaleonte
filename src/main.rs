#[macro_use]
extern crate rocket;
use rocket::{ State, fs::{ FileServer, NamedFile } };

use rand::prelude::*;

use std::collections::{ HashSet, HashMap, };
use std::sync::Mutex;
use std::time::Instant;

static WAIT_TIME: f32 = 5.0;

struct RoomState {
    players: HashSet<u64>,
    chameleon: u64,
    code: Option<String>,
    last_update: Instant,
}

impl RoomState {
    fn new(first_player: u64) -> Self {
        let mut players = HashSet::with_capacity(20);
        players.insert(first_player);
        RoomState {
            players,
            chameleon: 0,
            code: None,
            last_update: Instant::now(),
        }
    }
}

#[derive(Default)]
struct ServerState {
    rooms: Mutex<HashMap<String, RoomState>>,
}

#[get("/<_room>/<_user>")]
async fn lobby(_room: String, _user: u64) -> Option<NamedFile> {
    NamedFile::open("static/room.html").await.ok()
}

#[post("/<room>/<user>/j")]
fn join(room: String, user: u64, state: &State<ServerState>) {
    let mut rooms = state.rooms.lock().unwrap();
    if let Some(room_state) = rooms.get_mut(&room) {
        room_state.players.insert(user);
        room_state.code = None;
        room_state.last_update = Instant::now();
    } else {
        rooms.insert(room, RoomState::new(user));
    }
}

#[get("/<room>/<user>/c")]
fn code(room: String, user: u64, state: &State<ServerState>) -> Option<String> {
    let mut rooms = state.rooms.lock().unwrap();
    if let Some(room_state) = rooms.get_mut(&room) {
        let time_elapsed = room_state.last_update.elapsed().as_secs_f32();
        if time_elapsed > WAIT_TIME {
            let code = match room_state.code.clone() {
                None => {
                    let code = format!("{}{}",
                        ["A", "B", "C", "D"][random::<usize>() % 4],
                        random::<usize>() % 4 + 1
                    );
                    room_state.code = Some(code.clone());
                    room_state.chameleon = *room_state.players.iter().nth(
                        random::<usize>() % room_state.players.len()
                    ).unwrap();
                    room_state.players.clear();
                    code
                },
                Some(code) => code
            };
            if user != room_state.chameleon {
                Some(format!("{{\"code\": \"{}\"}}", code))
            } else {
                Some("{\"code\": \"🦎\"}".to_string())
            }
        } else {
            Some(format!("{{\"time_left\": {}}}", WAIT_TIME - time_elapsed))
        }
    } else {
        None
    }
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .manage(ServerState::default())
        .mount("/", routes![lobby, join, code])
        .mount("/", FileServer::from("static"))
}
